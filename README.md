Installation Instructions
Google Chrome / Microsoft Edge (Custom sites supported)

Download this repo as a ZIP file from GitHub.
Unzip the file and you should have a folder named bypass-paywalls-chrome-master.
In Chrome/Edge go to the extensions page (chrome://extensions or edge://extensions).
Enable Developer Mode.
Drag the bypass-paywalls-chrome-master folder anywhere on the page to import it (do not delete the folder afterwards).
Mozilla Firefox (Custom sites not supported)

Download and install the latest version
Notes

Every time you open Chrome it may warn you about running extensions in developer mode, just click ✕ to keep the extension enabled.
You will be logged out for any site you have checked.
This extension works best alongside the adblocker uBlock Origin.
The Firefox version supports automatic updates.